import requests
import html as html_m
from lxml import etree
import re
import pandas as pd

if __name__ == "__main__":
    response=requests.get('https://okgo.tw/storeview.html?id=11311')#爬蟲目標網址
    list_text=[]
    html=etree.HTML(response.text)
    location_text = ""
    Noneed=['<p>','</p>','<span>','</span>','<?xml version="1.0" encoding="big5"?>','<div>','</div>','<br>','</br>','<b>','</b>','<font color="red" size="4">','</font>']
    #多餘標籤list
    for location in html.xpath('.//div[@class="textinfo"]/h3/a/text()'):#地址
        location_text = location_text + location
    print(location_text)
    list_text.append(location_text)
    for title in html.xpath('.//div[@class="textinfo"]/h2/text()'):#景點
        print(title)
    list_text.append(title)
    for Introduction in html.xpath('.//div[@id="b2"]'):#店家簡介
        Introduction_text = Introduction.xpath('.//div[@class="editor"]/p')[0]#已成list型態
        Introduction_string = etree.tostring(Introduction_text,method='html')#轉字串
        Introduction_string = Introduction_string.decode('utf8')#編碼
        for i in range(len(Noneed)):#清除掉多餘標籤
            if(Noneed[i] in Introduction_string):
                Introduction_string = Introduction_string.replace(Noneed[i],"")
        print(html_m.unescape(Introduction_string))#解碼輸出
    list_text.append(html_m.unescape(Introduction_string))   
    for date in html.xpath('.//div[@id="b3"]'):#營運時間
        date_text = date.xpath('.//div[@class="editor"]/div')[0]
        date_string = etree.tostring(date_text,method='html')
        date_string = date_string.decode('utf8')
        for i in range(len(Noneed)):#清除掉多餘標籤
            if(Noneed[i] in date_string):
                date_string = date_string.replace(Noneed[i],"")
        print(html_m.unescape(date_string))
    list_text.append(html_m.unescape(date_string))
    for fare in html.xpath('.//div[@id="b5"]'):#票價資訊
        fare_text = fare.xpath('.//div[@class="editor"]/div')[0]
        fare_string = etree.tostring(fare_text,method='html')
        fare_string = fare_string.decode('utf8')
        for i in range(len(Noneed)):#清除掉多餘標籤
            if(Noneed[i] in fare_string):
              fare_string = fare_string.replace(Noneed[i],"")
        print(html_m.unescape(fare_string))
    list_text.append(html_m.unescape(fare_string))
    print(list_text)
    file_path = './result.csv'
    dic_title=[{'地址':location_text,'景點':title,'店家簡介':html_m.unescape(Introduction_string),'營運時間':html_m.unescape(date_string),'票價資訊':html_m.unescape(fare_string)}]#欄位
    df = pd.DataFrame(dic_title)
    df.to_csv(file_path,index=False,sep=',',encoding='utf_8_sig')